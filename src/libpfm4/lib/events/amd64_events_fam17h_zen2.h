/*
 * Contributed by Stephane Eranian <eranian@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * This file is part of libpfm, a performance monitoring support library for
 * applications on Linux.
 *
 * PMU: amd64_fam17h_zen2_zen2 (AMD64 Fam17h Zen2))
 */

static const amd64_umask_t amd64_fam17h_zen2_l1_itlb_miss_l2_itlb_miss[]={
  { .uname  = "IF1G",
    .udesc  = "Number of instruction fetches to a 1GB page",
    .ucode  = 0x4,
  },
  { .uname  = "IF2M",
    .udesc  = "Number of instruction fetches to a 2MB page",
    .ucode  = 0x2,
  },
  { .uname  = "IF4K",
    .udesc  = "Number of instruction fetches to a 4KB page",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_itlb_fetch_hit[]={
  { .uname  = "IF1G",
    .udesc  = "L1 instruction fetch that hit a 1GB page.",
    .ucode  = 0x4,
  },
  { .uname  = "IF2M",
    .udesc  = "L1 instruction fetch that hit a 2MB page.",
    .ucode  = 0x2,
  },
  { .uname  = "IF4K",
    .udesc  = "L1 instruction fetch that hit a 4KB page.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_retired_mmx_fp_instructions[]={
  { .uname  = "SSE_INSTR",
    .udesc  = "Number of SSE instructions (SSE, SSE2, SSE3, SSE$, SSE4A, SSE41, SSE42, AVX).",
    .ucode  = 0x4,
  },
  { .uname  = "MMX_INSTR",
    .udesc  = "Number of MMX instructions.",
    .ucode  = 0x2,
  },
  { .uname  = "X87_INSTR",
    .udesc  = "Number of X87 instructions.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_tagged_ibs_ops[]={
  { .uname  = "IBS_COUNT_ROLLOVER",
    .udesc  = "Number of times a uop could not be tagged by IBS because of a previous tagged uop that has not retired.",
    .ucode  = 0x4,
  },
  { .uname  = "IBS_TAGGED_OPS_RET",
    .udesc  = "Number of uops tagged by IBS that retired.",
    .ucode  = 0x2,
  },
  { .uname  = "IBS_TAGGED_OPS",
    .udesc  = "Number of uops tagged by IBS.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_number_of_move_elimination_and_scalar_op_optimization[]={
  { .uname  = "OPTIMIZED",
    .udesc  = "Number of scalar ops optimized.",
    .ucode  = 0x8,
  },
  { .uname  = "OPT_POTENTIAL",
    .udesc  = "Number of ops that are candidates for optimization (have z-bit either set or pass.",
    .ucode  = 0x4,
  },
  { .uname  = "SSE_MOV_OPS_ELIM",
    .udesc  = "Number of SSE move ops eliminated.",
    .ucode  = 0x2,
  },
  { .uname  = "SSE_MOV_OPS",
    .udesc  = "Number of SSE move ops.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_retired_sse_avx_operations[]={
  { .uname  = "MAC_FLOPS",
    .udesc  = "Mac flops. MAC FLOPS count as 2 FLOPS.",
    .ucode  = 0x8,
  },
  { .uname  = "DIV_FLOPS",
    .udesc  = "Divide/square root flops.",
    .ucode  = 0x4,
  },
  { .uname  = "MULT_FLOPS",
    .udesc  = "Multiply flops.",
    .ucode  = 0x2,
  },
  { .uname  = "ADD_SUB_FLOPS",
    .udesc  = "Add/subtract flops.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_retired_serializing_ops[]={
  { .uname  = "X87_CTRL_RET",
    .udesc  = "X87 control word mispredict traps due to mispredction in RC or PC, or changes in mask bits.",
    .ucode  = 0x1,
  },
  { .uname  = "X87_BOT_RET",
    .udesc  = "X87 bottom-executing uops retired.",
    .ucode  = 0x2,
  },
  { .uname  = "SSE_CTRL_RET",
    .udesc  = "SSE control word mispreduct traps due to mispredctions in RC, FTZ or DAZ or changes in mask bits.",
    .ucode  = 0x4,
  },
  { .uname  = "SSE_BOT_RET",
    .udesc  = "SSE bottom-executing uops retired.",
    .ucode  = 0x8,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_fp_dispatch_faults[]={
  { .uname  = "X87_FULL_FAULT",
    .udesc  = "X87 fill faults",
    .ucode  = 0x1,
  },
  { .uname  = "XMM_FILL_FAULT",
    .udesc  = "XMM fill faults",
    .ucode  = 0x2,
  },
  { .uname  = "YMM_FILL_FAULT",
    .udesc  = "YMM fill faults",
    .ucode  = 0x4,
  },
  { .uname  = "YMM_SPILL_FAULT",
    .udesc  = "YMM spill faults",
    .ucode  = 0x8,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_retired_x87_floating_point_operations[]={
  { .uname  = "DIV_SQR_R_OPS",
    .udesc  = "Divide and square root ops",
    .ucode  = 0x4,
  },
  { .uname  = "MUL_OPS",
    .udesc  = "Multiple ops",
    .ucode  = 0x2,
  },
  { .uname  = "ADD_SUB_OPS",
    .udesc  = "Add/subtract ops",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_fpu_pipe_assignment[]={
  { .uname  = "DUAL3",
    .udesc  = "Total number of multi-pipe uops assigned to pipe3",
    .ucode  = 0x80,
  },
  { .uname  = "DUAL2",
    .udesc  = "Total number of multi-pipe uops assigned to pipe2",
    .ucode  = 0x40,
  },
  { .uname  = "DUAL1",
    .udesc  = "Total number of multi-pipe uops assigned to pipe1",
    .ucode  = 0x20,
  },
  { .uname  = "DUAL0",
    .udesc  = "Total number of multi-pipe uops assigned to pipe0",
    .ucode  = 0x10,
  },
  { .uname  = "TOTAL3",
    .udesc  = "Total number of uops assigned to pipe3",
    .ucode  = 0x8,
  },
  { .uname  = "TOTAL2",
    .udesc  = "Total number of uops assigned to pipe2",
    .ucode  = 0x4,
  },
  { .uname  = "TOTAL1",
    .udesc  = "Total number of uops assigned to pipe1",
    .ucode  = 0x2,
  },
  { .uname  = "TOTAL0",
    .udesc  = "Total number of uops assigned to pipe0",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_instruction_cache_lines_invalidated[]={
  { .uname  = "L2_INVALIDATING_PROBE",
    .udesc  = "IC line invalidated due to L2 invalidating probe (external or LS).",
    .ucode  = 0x2,
  },
  { .uname  = "FILL_INVALIDATED",
    .udesc  = "IC line invalidated due to overwriting fill response.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_instruction_pipe_stall[]={
  { .uname  = "IC_STALL_ANY",
    .udesc  = "IC pipe was stalled during this clock cycle for any reason (nothing valud in pipe ICM1).",
    .ucode  = 0x4,
  },
  { .uname  = "IC_STALL_DQ_EMPTY",
    .udesc  = "IC pipe was stalled during this clock cycle (including IC to OC fetches) due to DQ empty.",
    .ucode  = 0x2,
  },
  { .uname  = "IC_STALL_BACK_PRESSURE",
    .udesc  = "IC pipe was stalled during this clock cycle (ncluding IC to OC fetches) due to back pressure.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_core_to_l2_cacheable_request_access_status[]={
  { .uname  = "LS_RD_BLK_C_S",
    .udesc  = "Number of data cache shared read hitting in the L2.",
    .ucode  = 0x80,
  },
  { .uname  = "LS_RD_BLK_L_HIT_X",
    .udesc  = "Number of data cache reads hitting in the L2.",
    .ucode  = 0x40,
  },
  { .uname  = "LS_RD_BLK_L_HIT_S",
    .udesc  = "Number of data cache reads hitting a shared in line in the L2.",
    .ucode  = 0x20,
  },
  { .uname  = "LS_RD_BLK_X",
    .udesc  = "Number of data cache store or state change (to exclusive) requests hitting in the L2.",
    .ucode  = 0x10,
  },
  { .uname  = "LS_RD_BLK_C",
    .udesc  = "Number of data cache fill requests missing in the L2 (all types).",
    .ucode  = 0x8,
  },
  { .uname  = "IC_FILL_HIT_X",
    .udesc  = "Number of I-cache fill requests hitting a modifiable (exclusive) line in the L2.",
    .ucode  = 0x4,
  },
  { .uname  = "IC_FILL_HIT_S",
    .udesc  = "Number of I-cache fill requests hitting a clean line in the L2.",
    .ucode  = 0x2,
  },
  { .uname  = "IC_FILL_MISS",
    .udesc  = "Number of I-cache fill requests missing the L2.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_l2_prefetch_hit_l2[]={
  { .uname  = "ANY",
    .udesc  = "Any L2 prefetch requests",
    .ucode  = 0x1f,
    .uflags = AMD64_FL_DFL,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_cycles_with_fill_pending_from_l2[]={
  { .uname  = "L2_FILL_BUSY",
   .udesc  = "TBD",
    .ucode  = 0x1,
    .uflags = AMD64_FL_DFL,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_l2_latency[]={
  { .uname  = "L2_CYCLES_WAITING_ON_FILLS",
    .udesc  = "TBD",
    .ucode  = 0x1,
    .uflags = AMD64_FL_DFL,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_requests_to_l2_group1[]={
  { .uname  = "RD_BLK_L",
    .udesc  = "Number of data cache reads (including software and hardware prefetches).",
    .ucode  = 0x80,
  },
  { .uname  = "RD_BLK_X",
    .udesc  = "Number of data cache stores",
    .ucode  = 0x40,
  },
  { .uname  = "LS_RD_BLK_C_S",
    .udesc  = "Number of data cache shared reads.",
    .ucode  = 0x20,
  },
  { .uname  = "CACHEABLE_IC_READ",
    .udesc  = "Number of instruction cache reads.",
    .ucode  = 0x10,
  },
  { .uname  = "CHANGE_TO_X",
    .udesc  = "Number of requests change to writable. Check L2 for current state.",
    .ucode  = 0x8,
  },
  { .uname  = "PREFETCH_L2",
    .udesc  = "TBD",
    .ucode  = 0x4,
  },
  { .uname  = "L2_HW_PF",
    .udesc  = "Number of prefetches accepted by L2 pipeline, hit or miss.",
    .ucode  = 0x2,
  },
  { .uname  = "GROUP2",
    .udesc  = "Number of miscellaneous requests covered in more details by REQUESTS_TO_L2_GROUP1",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_requests_to_l2_group2[]={
  { .uname  = "GROUP1",
    .udesc  = "Number of miscellaneous requests covered in more details by REQUESTS_TO_L2_GROUP2",
    .ucode  = 0x80,
  },
  { .uname  = "LS_RD_SIZED",
    .udesc  = "Number of data cache reads sized.",
    .ucode  = 0x40,
  },
  { .uname  = "LS_RD_SIZED_N_C",
    .udesc  = "Number of data cache reads sized non-cacheable.",
    .ucode  = 0x20,
  },
  { .uname  = "IC_RD_SIZED",
    .udesc  = "Number of instruction cache reads sized.",
    .ucode  = 0x10,
  },
  { .uname  = "IC_RD_SIZED_N_C",
    .udesc  = "Number of instruction cache reads sized non-cacheable.",
    .ucode  = 0x8,
  },
  { .uname  = "SMC_INVAL",
    .udesc  = "Number of self-modifying code invalidates.",
    .ucode  = 0x4,
  },
  { .uname  = "BUS_LOCKS_ORIGINATOR",
    .udesc  = "Number of bus locks.",
    .ucode  = 0x2,
  },
  { .uname  = "BUS_LOCKS_RESPONSES",
    .udesc  = "Number of bus lock responses.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_ls_to_l2_wbc_requests[]={
  { .uname  = "WCB_WRITE",
    .udesc  = "TBD",
    .ucode  = 0x40,
  },
  { .uname  = "WCB_CLOSE",
    .udesc  = "TBD",
    .ucode  = 0x20,
  },
  { .uname  = "CACHE_LINE_FLUSH",
    .udesc  = "TBD",
    .ucode  = 0x10,
  },
  { .uname  = "I_LINE_FLUSH",
    .udesc  = "TBD",
    .ucode  = 0x8,
  },
  { .uname  = "ZERO_BYTE_STORE",
    .udesc  = "TBD",
    .ucode  = 0x4,
  },
  { .uname  = "LOCAL_IC_CLR",
    .udesc  = "TBD",
    .ucode  = 0x2,
  },
  { .uname  = "C_L_ZERO",
    .udesc  = "TBD",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_bad_status_2[]={
  { .uname  = "STLI_OTHER",
    .udesc  = "Store-to-load conflicts. A load was unable to complete due to a non-forwardable conflict with an older store.",
    .ucode  = 0x2,
    .uflags = AMD64_FL_DFL,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_retired_lock_instructions[]={
  { .uname  = "CACHEABLE_LOCKS",
    .udesc  = "Lock in cacheable memory region.",
    .ucode  = 0xe,
    .uflags = AMD64_FL_DFL,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_tlb_flushes[]={
  { .uname  = "ANY",
    .udesc  = "ANY TLB flush.",
    .ucode  = 0xff,
    .uflags = AMD64_FL_DFL,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_ls_dispatch[]={
  { .uname  = "LD_ST_DISPATCH",
    .udesc  = "Load/Store single uops dispatched (compare-and-exchange).",
    .ucode  = 0x4,
  },
  { .uname  = "STORE_DISPATCH",
    .udesc  = "Store uops dispatched.",
    .ucode  = 0x2,
  },
  { .uname  = "LD_DISPATCH",
    .udesc  = "Load uops dispatched.",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_ineffective_software_prefetch[]={
  { .uname  = "MAB_MCH_CNT",
    .udesc  = "Software prefetch instructions saw a match on an already allocated miss request buffer.",
    .ucode  = 0x2,
  },
  { .uname  = "DATA_PIPE_SW_PF_DC_HIT",
    .udesc  = "Software Prefetch instruction saw a DC hit",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_software_prefetch_data_cache_fills[]={
  { .uname  = "MABRESP_LCL_L2",
    .udesc  = "Fill from local L2.",
    .ucode  = 0x1,
  },
  { .uname  = "LS_MABRESP_LCL_CACHE",
    .udesc  = "Fill from another cache (home node local).",
    .ucode  = 0x2,
  },
  { .uname  = "LS_MABRESP_LCL_DRAM",
    .udesc  = "Fill from DRAM (home node local).",
    .ucode  = 0x8,
  },
  { .uname  = "LS_MABRESP_LCL_RMT_CACHE",
    .udesc  = "Fill from another cache (home node remote).",
    .ucode  = 0x10,
  },
  { .uname  = "LS_MABRESP_LCL_RMT_DRAM",
    .udesc  = "Fill from DRAM (home node remote).",
    .ucode  = 0x40,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_store_commit_cancels_2[]={
  { .uname  = "WCB_FULL",
    .udesc  = "Non cacheable store and the non-cacheable commit buffer is full.",
    .ucode  = 0x1,
    .uflags = AMD64_FL_DFL,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_l1_dtlb_miss[]={
  { .uname  = "TLB_RELOAD_1G_L2_MISS",
    .udesc  = "Data TLB reload to a 1GB page that missed in the L2 TLB",
    .ucode  = 0x80,
  },
  { .uname  = "TLB_RELOAD_2M_L2_MISS",
    .udesc  = "Data TLB reload to a 2MB page that missed in the L2 TLB",
    .ucode  = 0x40,
  },
  { .uname  = "TLB_RELOAD_COALESCED_PAGE_MISS",
    .udesc  = "Data TLB reload to coalesced pages that missed",
    .ucode  = 0x20,
  },
  { .uname  = "TLB_RELOAD_4K_L2_MISS",
    .udesc  = "Data TLB reload to a 4KB page that missed in the L2 TLB",
    .ucode  = 0x10,
  },
  { .uname  = "TLB_RELOAD_1G_L2_HIT",
    .udesc  = "Data TLB reload to a 1GB page that hit in the L2 TLB",
    .ucode  = 0x8,
  },
  { .uname  = "TLB_RELOAD_2M_L2_HIT",
    .udesc  = "Data TLB reload to a 2MB page that hit in the L2 TLB",
    .ucode  = 0x4,
  },
  { .uname  = "TLB_RELOAD_COALESCED_PAGE_HIT",
    .udesc  = "Data TLB reload to coalesced pages that hit",
    .ucode  = 0x2,
  },
  { .uname  = "TLB_RELOAD_4K_L2_HIT",
    .udesc  = "Data TLB reload to a 4KB page thta hit in the L2 TLB",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_locks[]={
  { .uname  = "SPEC_LOCK_MAP_COMMIT",
    .udesc  = "TBD",
    .ucode  = 0x8,
  },
  { .uname  = "SPEC_LOCK",
    .udesc  = "TBD",
    .ucode  = 0x4,
  },
  { .uname  = "NON_SPEC_LOCK",
    .udesc  = "TBD",
    .ucode  = 0x2,
  },
  { .uname  = "BUS_LOCK",
    .udesc  = "TBD",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_mab_allocation_by_pipe[]={
  { .uname  = "TLB_PIPE_EARLY",
    .udesc  = "TBD",
    .ucode  = 0x10,
  },
  { .uname  = "HW_PF",
    .udesc  = "hw_pf",
    .ucode  = 0x8,
  },
  { .uname  = "TLB_PIPE_LATE",
    .udesc  = "TBD",
    .ucode  = 0x4,
  },
  { .uname  = "ST_PIPE",
    .udesc  = "TBD",
    .ucode  = 0x2,
  },
  { .uname  = "DATA_PIPE",
    .udesc  = "TBD",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_prefetch_instructions_dispatched[]={
  { .uname  = "ANY",
    .udesc  = "Any prefetch",
    .ucode  = 0xff,
    .uflags = AMD64_FL_DFL,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_tablewalker_allocation[]={
  { .uname  = "ALLOC_ISIDE1",
    .udesc  = "TBD",
    .ucode  = 0x8,
  },
  { .uname  = "ALLOC_ISIDE0",
    .udesc  = "TBD",
    .ucode  = 0x4,
  },
  { .uname  = "ALLOC_DSIDE1",
    .udesc  = "TBD",
    .ucode  = 0x2,
  },
  { .uname  = "ALLOC_DSIDE0",
    .udesc  = "TBD",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_oc_mode_switch[]={
  { .uname  = "OC_IC_MODE_SWITCH",
    .udesc  = "TBD",
    .ucode  = 0x2,
  },
  { .uname  = "IC_OC_MODE_SWITCH",
    .udesc  = "TBD",
    .ucode  = 0x1,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_uops_dispatched_from_decoder[]={
  { .uname  = "DECODER_DISPATCHED",
    .udesc  = "Number of uops dispatched from the Decoder",
    .ucode  = 0x1,
  },
  { .uname  = "OPCACHE_DISPATCHED",
    .udesc  = "Number of uops dispatched from the OpCache",
    .ucode  = 0x2,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_dispatch_resource_stall_cycles_1[]={
  { .uname  = "INT_PHY_REG_FILE_RSRC_STALL",
    .udesc  = "Number of cycles stalled due to integer physical register file resource stalls. Applies to all uops that have integer destination register.",
    .ucode  = 0x1,
  },
  { .uname  = "LOAD_QUEUE_RSRC_STALL",
    .udesc  = "Number of cycles stalled due to load queue resource stalls. Applies to all uops with load semantics.",
    .ucode  = 0x2,
  },
  { .uname  = "STORE_QUEUE_RSRC_STALL",
    .udesc  = "Number of cycles stalled due to store queue resource stalls. Applies to all uops with store semantics.",
    .ucode  = 0x4,
  },
  { .uname  = "INT_SCHEDULER_MISC_RSRC_STALL",
    .udesc  = "Number of cycles stalled due to integer scheduler miscellaneous resource stalls.",
    .ucode  = 0x8,
  },
  { .uname  = "TAKEN_BRANCH_BUFFER_RSRC_STALL",
    .udesc  = "Number of cycles stalled due to taken branch buffer resource stalls.",
    .ucode  = 0x10,
  },
  { .uname  = "FP_REG_FILE_RSRC_STALL",
    .udesc  = "Number of cycles stalled due to floating-point register file resource stalls.",
    .ucode  = 0x20,
  },
  { .uname  = "FP_SCHEDULER_FILE_RSRC_STALL",
    .udesc  = "Number of cycles stalled due to floating-point scheduler resource stalls.",
    .ucode  = 0x40,
  },
  { .uname  = "FP_MISC_FILE_RSRC_STALL",
    .udesc  = "Number of cycles stalled due to floating-point miscellaneous resource unavailable.",
    .ucode  = 0x80,
  },
};

static const amd64_umask_t amd64_fam17h_zen2_dispatch_resource_stall_cycles_0[]={
  { .uname  = "ALU_TOKEN_STALL",
    .udesc  = "Number of cycles ALU tokens total unavailable.",
    .ucode  = 0x8,
    .uflags = AMD64_FL_DFL,
  },
};

static const amd64_entry_t amd64_fam17h_zen2_pe[]={
  { .name   = "L1_ITLB_MISS_L2_ITLB_HIT",
    .desc   = "Number of instruction fetches that miss in the L1 ITLB but hit in the L2 ITLB.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x84,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "L1_ITLB_MISS_L2_ITLB_MISS",
    .desc   = "Number of instruction fetches that miss in both the L1 and L2 TLBs.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x85,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_l1_itlb_miss_l2_itlb_miss),
    .umasks = amd64_fam17h_zen2_l1_itlb_miss_l2_itlb_miss,
  },
  { .name   = "DIV_CYCLES_BUSY_COUNT",
    .desc   = "Number of cycles when the divider is busy.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xd3,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "DIV_OP_COUNT",
    .desc   = "Number of divide uops.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xd4,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_BRANCH_INSTRUCTIONS",
    .desc   = "Number of branch instructions retired. This includes all types of architectural control flow changes, including exceptions and interrupts.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xc2,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_FAR_CONTROL_TRANSFERS",
    .desc   = "Number of far control transfers retired including far call/jump/return, IRET, SYSCALL and SYSRET, plus exceptions and interrupts. Far control transfers are not subject to branch prediction.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xc6,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_INDIRECT_BRANCH_INSTRUCTIONS_MISPREDICTED",
    .desc   = "Number of indirect branches retired there were not correctly predicted. Each such mispredict incurs the same penalty as a mispredicted condition branch instruction. Only EX mispredicts are counted.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xca,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_BRANCH_INSTRUCTIONS_MISPREDICTED",
    .desc   = "Number of branch instructions retired, of any type, that were not correctly predicted. This includes those for which prediction is not attempted (far control transfers, exceptions and interrupts).",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xc3,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_TAKEN_BRANCH_INSTRUCTIONS",
    .desc   = "Number of taken branches that were retired. This includes all types of architectural control flow changes, including exceptions and interrupts.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xc4,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_TAKEN_BRANCH_INSTRUCTIONS_MISPREDICTED",
    .desc   = "Number of retired taken branch instructions that were mispredicted.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xc5,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_CONDITIONAL_BRANCH_INSTRUCTIONS",
    .desc   = "Number of retired conditional branch instructions.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xd1,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_UOPS",
    .desc   = "Number of uops retired. This includes all processor activity (instructions, exceptions, interrupts, microcode assists, etc.). The number of events logged per cycle can vary from 0 to 8.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xc1,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_FUSED_INSTRUCTIONS",
    .desc   = "Number of fused retired branch instructions retired per cycle. The number of events logged per cycle can vary from 0 to 3.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x1d0,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_INSTRUCTIONS",
    .desc   = "Instructions Retired.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xc0,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_MMX_FP_INSTRUCTIONS",
    .desc   = "Number of MMX, SSE or x87 instructions retired. The UnitMask allows the selection of the individual classes of instructions as given in the table. Each increment represents one complete instruction. Since this event includes non-numeric instructions, it is not suitable for measuring MFLOPS.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xcb,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_retired_mmx_fp_instructions),
    .umasks = amd64_fam17h_zen2_retired_mmx_fp_instructions,
  },
  { .name   = "RETIRED_NEAR_RETURNS",
    .desc   = "Number of near return instructions (RET or RETI) retired.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xc8,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "RETIRED_NEAR_RETURNS_MISPREDICTED",
    .desc   = "Number of near returns retired that were not correctly predicted by the return address predictor. Each such mispredict incurs the same penalty as a mispredicted conditional branch instruction.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xc9,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "TAGGED_IBS_OPS",
    .desc   = "TBD",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x1cf,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_tagged_ibs_ops),
    .umasks = amd64_fam17h_zen2_tagged_ibs_ops,
  },
  { .name   = "INSTRUCTION_CACHE_REFILLS_FROM_L2",
    .desc   = "Number of 64-byte instruction cachelines that was fulfilled by the L2 cache.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x82,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "INSTRUCTION_CACHE_REFILLS_FROM_SYSTEM",
    .desc   = "Number of 64-byte instruction cachelines fulfilled from system memory or another cache.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x83,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "CORE_TO_L2_CACHEABLE_REQUEST_ACCESS_STATUS",
    .desc   = "L2 cache request outcomes. This event does not count accesses to the L2 cache by the L2 prefetcher.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x64,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_core_to_l2_cacheable_request_access_status),
    .umasks = amd64_fam17h_zen2_core_to_l2_cacheable_request_access_status,
  },
  { .name   = "L2_PREFETCH_HIT_L2",
    .desc   = "Number of L2 prefetcher hits in the L2",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x70,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_l2_prefetch_hit_l2),
    .umasks = amd64_fam17h_zen2_l2_prefetch_hit_l2,
  },
  { .name   = "L2_PREFETCH_HIT_L3",
    .desc   = "Number of L2 prefetcher hits in the L3",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x71,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_l2_prefetch_hit_l2),
    .umasks = amd64_fam17h_zen2_l2_prefetch_hit_l2, /* shared */
  },
  { .name   = "REQUESTS_TO_L2_GROUP1",
    .desc   = "TBD",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x60,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_requests_to_l2_group1),
    .umasks = amd64_fam17h_zen2_requests_to_l2_group1,
  },
  { .name   = "REQUESTS_TO_L2_GROUP2",
    .desc   = "Multi-events in that LS and IF requests can be received simultaneous.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x61,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_requests_to_l2_group2),
    .umasks = amd64_fam17h_zen2_requests_to_l2_group2,
  },
  { .name   = "BAD_STATUS_2",
    .desc   = "TBD",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x24,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_bad_status_2),
    .umasks = amd64_fam17h_zen2_bad_status_2,
  },
  { .name   = "LS_DISPATCH",
    .desc   = "Counts the number of operations dispatched to the LS unit. Unit Masks ADDed.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x29,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_ls_dispatch),
    .umasks = amd64_fam17h_zen2_ls_dispatch,
  },
  { .name   = "INEFFECTIVE_SOFTWARE_PREFETCH",
    .desc   = "Number of software prefetches that did not fetch data outside of the processor core.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x52,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_ineffective_software_prefetch),
    .umasks = amd64_fam17h_zen2_ineffective_software_prefetch,
  },
  { .name   = "SOFTWARE_PREFETCH_DATA_CACHE_FILLS",
    .desc   = "Number of software prefetches fills by data source",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x59,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_software_prefetch_data_cache_fills),
    .umasks = amd64_fam17h_zen2_software_prefetch_data_cache_fills,
  },
  { .name   = "HARDWARE_PREFETCH_DATA_CACHE_FILLS",
    .desc   = "Number of hardware prefetches fills by data source",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x5a,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_software_prefetch_data_cache_fills),
    .umasks = amd64_fam17h_zen2_software_prefetch_data_cache_fills, /* shared */
  },
  { .name   = "L1_DTLB_MISS",
    .desc   = "L1 Data TLB misses.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x45,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_l1_dtlb_miss),
    .umasks = amd64_fam17h_zen2_l1_dtlb_miss,
  },
  { .name   = "RETIRED_LOCK_INSTRUCTIONS",
    .desc   = "Counts the number of retired locked instructions",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x25,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_retired_lock_instructions),
    .umasks = amd64_fam17h_zen2_retired_lock_instructions,
  },
  { .name   = "RETIRED_CLFLUSH_INSTRUCTIONS",
    .desc   = "Counts the number of retired non-speculative clflush instructions",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x26,
    .flags   = 0,
  },
  { .name   = "RETIRED_CPUID_INSTRUCTIONS",
    .desc   = "Counts the number of retired cpuid instructions",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x27,
    .flags   = 0,
  },
  { .name   = "SMI_RECEIVED",
    .desc   = "Counts the number system management interrupts (SMI) received",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x2b,
    .flags   = 0,
  },
  { .name   = "INTERRUPT_TAKEN",
    .desc   = "Counts the number of interrupts taken",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x2c,
    .flags   = 0,
  },
  { .name   = "MAB_ALLOCATION_BY_PIPE",
    .desc   = "TBD",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x41,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_mab_allocation_by_pipe),
    .umasks = amd64_fam17h_zen2_mab_allocation_by_pipe,
  },
  { .name   = "MISALIGNED_LOADS",
    .desc   = "Misaligned loads retired",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x47,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "CYCLES_NOT_IN_HALT",
    .desc   = "Number of core cycles not in halted state",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x76,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "TLB_FLUSHES",
    .desc   = "Number of TLB flushes",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x78,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_tlb_flushes),
    .umasks = amd64_fam17h_zen2_tlb_flushes,
  },
  { .name   = "PREFETCH_INSTRUCTIONS_DISPATCHED",
    .desc   = "Software Prefetch Instructions Dispatched. This is a speculative event",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x4b,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_prefetch_instructions_dispatched),
    .umasks = amd64_fam17h_zen2_prefetch_instructions_dispatched,
  },
  { .name   = "STORE_TO_LOAD_FORWARD",
    .desc   = "Number of STore Lad Forward hits.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x35,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "STORE_COMMIT_CANCELS_2",
    .desc   = "Number of store commit cancellations",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x37,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_store_commit_cancels_2),
    .umasks = amd64_fam17h_zen2_store_commit_cancels_2,
  },
  { .name   = "L1_BTB_CORRECTION",
    .desc   = "Number of L1 branch prediction overrides of existing prediction. This is a speculative event.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x8a,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "L2_BTB_CORRECTION",
    .desc   = "Number of L2 branch prediction overrides of existing prediction. This is a speculative event.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x8b,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "DYNAMIC_INDIRECT_PREDICTIONS",
    .desc   = "Number of indirect branch prediction for potential multi-target branch. This is a speculative event.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x8e,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "DECODER_OVERRIDE_BRANCH_PRED",
    .desc   = "Numbner of decoder overrides of existing brnach prediction. This is a speculative event.",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x91,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "ITLB_FETCH_HIT",
    .desc   = "Instruction fetches that hit in the L1 ITLB",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0x94,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_itlb_fetch_hit),
    .umasks = amd64_fam17h_zen2_itlb_fetch_hit,
  },
  { .name   = "UOPS_QUEUE_EMPTY",
    .desc   = "Cycles where the uops queue is empty",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xa9,
    .flags   = 0,
    .ngrp    = 0,
  },
  { .name   = "UOPS_DISPATCHED_FROM_DECODER",
    .desc   = "Number of uops dispatched from either the Decoder, OpCache or both",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xaa,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_uops_dispatched_from_decoder),
    .umasks = amd64_fam17h_zen2_uops_dispatched_from_decoder,
  },
  { .name   = "DISPATCH_RESOURCE_STALL_CYCLES_1",
    .desc   = "Number of cycles where a dispatch group is valid but does not get dispatched due to a Token Stall",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xae,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_dispatch_resource_stall_cycles_1),
    .umasks = amd64_fam17h_zen2_dispatch_resource_stall_cycles_1,
  },
  { .name   = "DISPATCH_RESOURCE_STALL_CYCLES_0",
    .desc   = "Number of cycles where a dispatch group is valid but does not get dispatched due to a Token Stall",
    .modmsk  = AMD64_FAM17H_ATTRS,
    .code    = 0xaf,
    .flags   = 0,
    .ngrp    = 1,
    .numasks = LIBPFM_ARRAY_SIZE(amd64_fam17h_zen2_dispatch_resource_stall_cycles_0),
    .umasks = amd64_fam17h_zen2_dispatch_resource_stall_cycles_0,
  },
};
